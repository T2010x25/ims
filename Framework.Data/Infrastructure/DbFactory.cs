﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        ImsContext dbContext;

        public ImsContext Init()
        {
            return dbContext ?? (dbContext = new ImsContext());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
            {
                dbContext.Dispose();
            }
        }
    }
}
