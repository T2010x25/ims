﻿using Framework.Data.Repositories;
using Framework.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Data.Extensions
{
    public static class UserExtensions
    {
        public static AppUsers GetSingleByUsername(this IEntityBaseRepository<AppUsers> userRepository, string email)
        {
            return userRepository.GetAll().FirstOrDefault(x => x.EmailAddress== email);
        }
    }
}
