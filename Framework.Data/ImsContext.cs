﻿using Framework.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Data
{
   public class ImsContext:DbContext
    {
        public ImsContext():base("ImsConnection")
        {
            Database.SetInitializer<ImsContext>(null);
        }
        public IDbSet<Students> StudentsSet { get; set; }
        public IDbSet<AppUsers>AppUserSet { get; set; }
        public IDbSet<UserRoles>Roles { get; set; }
        public IDbSet<Enquiry>EnquirySet { get; set; }
        public IDbSet<ErrorLog>ErrorLogSet { get; set; }
        public IDbSet<Batches>BatchSet { get; set; }

        public IDbSet<Cities> CitySet { get; set; }
        public IDbSet<Courses> CourseSet{ get; set; }
        public IDbSet<Tracks>TrackSet { get; set; }
        public IDbSet<StatusMaster> StatusSet{ get; set; } 


        public void Commit()
        {
            base.SaveChanges();
        }
    }
}
