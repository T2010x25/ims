﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Framework.Data.Repositories;
using Framework.Entities;
using Framework.Data.Infrastructure;
using Framework.Services;

namespace Ims.Testing
{
    [TestClass]
    public class ImsUnitTesting
    {
        [TestMethod]
        public void StudentRepository_Given_Student_Id_Should_Get_Student_FirstName()
        {
            var studentid = 4;
            var firstname = "Vaibhar";

            IDbFactory dbcontext = new DbFactory();
            IEntityBaseRepository<Students> repo = new EntityBaseRepository<Students>(dbcontext);
            var students = repo.GetSingle(studentid);
            Assert.AreEqual(students.FirstName, firstname);
        }
        [TestMethod]
        public void Validate_User()
        {
            var email = "demo@disha.co.in";
            var password = "abcd1234";
            IDbFactory dbcontext = new DbFactory();

            IEntityBaseRepository<AppUsers> appuserRepo =
                new EntityBaseRepository<AppUsers>(dbcontext);

            IEntityBaseRepository<UserRoles> userroleRepo =
                new EntityBaseRepository<UserRoles>(dbcontext);

            IEncryptionService encryptionService =
                new EncryptionService();
            IUnitOfWork unitofWork = new
                            UnitOfWork(dbcontext);
            IAuthenticateService authService = new AuthenticateService
                        (appuserRepo, userroleRepo, encryptionService, unitofWork);
            var result = authService.ValidateUser(email, password);
            Assert.AreNotEqual(result.User, null );
        }

        [TestMethod]
        public void Check_Password_Hash()
        {
            var salt = "Zy1vyq73Z6DTga5DcBsv1A==";
            var password = "abcd1234";
            var expected = "DB+7oQuK5CySfviPMovoLsi8njSVVziYJkzSzOT/8jA=";

            IEncryptionService encryptionService =
                new EncryptionService();
           var output = encryptionService.EncryptPassword(password, salt);
            Assert.AreEqual(output, expected);
        }
    }
}
