CREATE DATABASE [ImsDataStore]
go
USE [ImsDataStore] 
GO
/****** Object:  Table [dbo].[AppUsers]    Script Date: 2/14/2017 2:24:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppUsers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EmailAddress] [varchar](100) NOT NULL,
	[PasswordHash] [varchar](100) NULL,
	[Salt] [varchar](50) NULL,
	[IsLocked] [bit] NULL,
	[DateCreated] [datetime] NULL,
	[RoleID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Batches]    Script Date: 2/14/2017 2:24:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Batches](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BatchName] [varchar](100) NULL,
	[CreateDate] [datetime] NOT NULL,
	[AddedBy] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cities]    Script Date: 2/14/2017 2:24:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cities](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CityName] [varchar](10) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[AddedBy] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Courses]    Script Date: 2/14/2017 2:24:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Courses](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CourseTitle] [varchar](100) NULL,
	[CreateDate] [datetime] NOT NULL,
	[AddedBy] [int] NOT NULL,
	[CourseDescription] [varchar](400) NULL,
	[IconUrl] [varchar](100) NULL,
	[StatusID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Enquiry]    Script Date: 2/14/2017 2:24:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Enquiry](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[DOB] [datetime] NOT NULL,
	[GenderID] [int] NOT NULL DEFAULT ((0)),
	[Address] [varchar](100) NULL,
	[Telephone] [varchar](50) NULL,
	[Mobile] [varchar](50) NULL,
	[EmailAddress] [varchar](50) NOT NULL,
	[CourseID] [int] NOT NULL,
	[Remarks] [varchar](100) NULL,
	[EnquiryDate] [datetime] NULL DEFAULT (getdate()),
	[StaffID] [int] NOT NULL DEFAULT ((0)),
	[StatusID] [int] NOT NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ErrorLog]    Script Date: 2/14/2017 2:24:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ErrorLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Message] [varchar](50) NULL,
	[StackTrace] [varchar](100) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Fee]    Script Date: 2/14/2017 2:24:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Fee](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CourseID] [varchar](10) NOT NULL,
	[StudentID] [int] NOT NULL,
	[Amount] [int] NOT NULL,
	[PaidDate] [datetime] NOT NULL,
	[AddedBy] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StatusMaster]    Script Date: 2/14/2017 2:24:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StatusMaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StatusTitle] [varchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Students]    Script Date: 2/14/2017 2:24:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Students](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](100) NOT NULL,
	[LastName] [nchar](100) NOT NULL,
	[DOB ] [datetime] NOT NULL,
	[GenderID] [int] NOT NULL,
	[Address] [varchar](100) NULL,
	[Telephone] [varchar](50) NULL,
	[Mobile] [varchar](50) NULL,
	[EmailAddress] [varchar](100) NULL,
	[CourseID] [int] NOT NULL,
	[StatusID] [int] NOT NULL DEFAULT ((10)),
	[FormNumber] [varchar](50) NOT NULL,
	[EnrollmentDate] [datetime] NOT NULL DEFAULT (getdate()),
	[CityID] [int] NULL,
	[QualificationID] [int] NULL,
	[BatchID ] [int] NULL,
	[TrackID ] [int] NULL,
	[JoiningDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tracks]    Script Date: 2/14/2017 2:24:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tracks](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TrackName] [varchar](100) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[AddedBy] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserRoles]    Script Date: 2/14/2017 2:24:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserRoles](
	[ID] [int] NOT NULL,
	[RoleName] [varchar](50) NOT NULL,
	[RoleDescription] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[AppUsers] ON 

INSERT [dbo].[AppUsers] ([ID], [EmailAddress], [PasswordHash], [Salt], [IsLocked], [DateCreated], [RoleID]) VALUES (1, N'demo@disha.co.in', N'DB+7oQuK5CySfviPMovoLsi8njSVVziYJkzSzOT/8jA=', N'Zy1vyq73Z6DTga5DcBsv1A==', 0, CAST(N'2017-02-12 19:45:14.737' AS DateTime), 1)
INSERT [dbo].[AppUsers] ([ID], [EmailAddress], [PasswordHash], [Salt], [IsLocked], [DateCreated], [RoleID]) VALUES (2, N'admin@disha.co.in', N'PY2QoWsqu2IDRjYBbHRIx0TF0Ljk6IhAPrNgaUdWPTU=', N'9YJ9odxXfp/+zV4VulM/Cw==', 0, CAST(N'2017-02-12 19:46:26.943' AS DateTime), 1)
INSERT [dbo].[AppUsers] ([ID], [EmailAddress], [PasswordHash], [Salt], [IsLocked], [DateCreated], [RoleID]) VALUES (3, N'operator@disha.co.in', N'3ZKvgCZozrfAHjH5Fj+p0TlVk+KTorLC0eBmHYiG6I4=', N'fuscRdPv2Zk77T8L/KhOTw==', 0, CAST(N'2017-02-12 19:47:07.513' AS DateTime), 2)
SET IDENTITY_INSERT [dbo].[AppUsers] OFF
SET IDENTITY_INSERT [dbo].[Batches] ON 

INSERT [dbo].[Batches] ([ID], [BatchName], [CreateDate], [AddedBy]) VALUES (2, N'7.00 - 8.00 AM', CAST(N'2017-02-12 03:20:17.657' AS DateTime), 1)
INSERT [dbo].[Batches] ([ID], [BatchName], [CreateDate], [AddedBy]) VALUES (3, N'9.00 - 10.00 AM', CAST(N'2017-02-12 03:20:33.650' AS DateTime), 1)
INSERT [dbo].[Batches] ([ID], [BatchName], [CreateDate], [AddedBy]) VALUES (4, N'11.00 - 12.00 AM', CAST(N'2017-02-12 03:21:09.780' AS DateTime), 1)
INSERT [dbo].[Batches] ([ID], [BatchName], [CreateDate], [AddedBy]) VALUES (5, N'11.30 - 12.30 PM', CAST(N'2017-02-12 03:21:32.970' AS DateTime), 1)
INSERT [dbo].[Batches] ([ID], [BatchName], [CreateDate], [AddedBy]) VALUES (6, N'12.00 - 13.30 PM', CAST(N'2017-02-12 03:21:57.793' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Batches] OFF
SET IDENTITY_INSERT [dbo].[Cities] ON 

INSERT [dbo].[Cities] ([ID], [CityName], [CreateDate], [AddedBy]) VALUES (1, N'Pune', CAST(N'2017-02-12 03:29:13.047' AS DateTime), 1)
INSERT [dbo].[Cities] ([ID], [CityName], [CreateDate], [AddedBy]) VALUES (2, N'Banglore', CAST(N'2017-02-12 03:29:13.103' AS DateTime), 1)
INSERT [dbo].[Cities] ([ID], [CityName], [CreateDate], [AddedBy]) VALUES (3, N'Chennai', CAST(N'2017-02-12 03:29:13.103' AS DateTime), 1)
INSERT [dbo].[Cities] ([ID], [CityName], [CreateDate], [AddedBy]) VALUES (4, N'Indore', CAST(N'2017-02-12 03:29:13.103' AS DateTime), 1)
INSERT [dbo].[Cities] ([ID], [CityName], [CreateDate], [AddedBy]) VALUES (5, N'Mumbai', CAST(N'2017-02-12 03:29:15.720' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Cities] OFF
SET IDENTITY_INSERT [dbo].[Courses] ON 

INSERT [dbo].[Courses] ([ID], [CourseTitle], [CreateDate], [AddedBy], [CourseDescription], [IconUrl], [StatusID]) VALUES (1, N'Computer Graphics', CAST(N'2017-02-12 03:31:02.107' AS DateTime), 1, N'Computer Graphics and Animations', N'cg.jpg', 101)
INSERT [dbo].[Courses] ([ID], [CourseTitle], [CreateDate], [AddedBy], [CourseDescription], [IconUrl], [StatusID]) VALUES (2, N'Java', CAST(N'2017-02-12 03:31:02.107' AS DateTime), 1, N'Programming in Java', N'jv.jpg', 101)
INSERT [dbo].[Courses] ([ID], [CourseTitle], [CreateDate], [AddedBy], [CourseDescription], [IconUrl], [StatusID]) VALUES (4, N'MS Excel', CAST(N'2017-02-12 03:31:02.107' AS DateTime), 1, N'Programming MS Excel VBA', N'ex.jpg', 101)
INSERT [dbo].[Courses] ([ID], [CourseTitle], [CreateDate], [AddedBy], [CourseDescription], [IconUrl], [StatusID]) VALUES (6, N'Python 2.0', CAST(N'2017-02-12 03:31:40.040' AS DateTime), 1, N'Programming in Python', N'py.jpg', 101)
INSERT [dbo].[Courses] ([ID], [CourseTitle], [CreateDate], [AddedBy], [CourseDescription], [IconUrl], [StatusID]) VALUES (7, N'AngularJS 2.0', CAST(N'2017-02-12 03:31:40.040' AS DateTime), 1, N'AngularJS 2 and Typescript', N'ag.jpg', 101)
INSERT [dbo].[Courses] ([ID], [CourseTitle], [CreateDate], [AddedBy], [CourseDescription], [IconUrl], [StatusID]) VALUES (8, N'Asp.NET MVC 5', CAST(N'2017-02-12 03:31:40.040' AS DateTime), 1, N'Web development using Asp.NET MVC 5', N'asp.jpg', 101)
INSERT [dbo].[Courses] ([ID], [CourseTitle], [CreateDate], [AddedBy], [CourseDescription], [IconUrl], [StatusID]) VALUES (9, N'MS Office 2015', CAST(N'2017-02-12 03:31:40.040' AS DateTime), 1, N'Learn to use MS Office 2015', N'ms.jpg', 101)
INSERT [dbo].[Courses] ([ID], [CourseTitle], [CreateDate], [AddedBy], [CourseDescription], [IconUrl], [StatusID]) VALUES (10, N'Adobe Photoshop 14', CAST(N'2017-02-12 03:31:40.040' AS DateTime), 1, N'Learn Photoshop Image Processing & Editing ', N'ph.jpg', 101)
SET IDENTITY_INSERT [dbo].[Courses] OFF
SET IDENTITY_INSERT [dbo].[Enquiry] ON 

INSERT [dbo].[Enquiry] ([ID], [FirstName], [LastName], [DOB], [GenderID], [Address], [Telephone], [Mobile], [EmailAddress], [CourseID], [Remarks], [EnquiryDate], [StaffID], [StatusID]) VALUES (1, N'Kishan', N'S', CAST(N'2010-01-01 00:00:00.000' AS DateTime), 1, N'MM', N'343', N'2323', N'mm@m.com', 2, N'Test', CAST(N'2017-02-02 00:00:00.000' AS DateTime), 1, 1)
INSERT [dbo].[Enquiry] ([ID], [FirstName], [LastName], [DOB], [GenderID], [Address], [Telephone], [Mobile], [EmailAddress], [CourseID], [Remarks], [EnquiryDate], [StaffID], [StatusID]) VALUES (2, N'dfsd', N'mopdodo', CAST(N'2010-01-01 00:00:00.000' AS DateTime), 1, N'sd', N'34324', N'3432', N'kishan.suryawanshi@gmail.com', 2, N'324', CAST(N'2017-02-11 16:40:24.150' AS DateTime), 0, 0)
INSERT [dbo].[Enquiry] ([ID], [FirstName], [LastName], [DOB], [GenderID], [Address], [Telephone], [Mobile], [EmailAddress], [CourseID], [Remarks], [EnquiryDate], [StaffID], [StatusID]) VALUES (3, N'dfsd', N'mopdodo', CAST(N'2010-01-01 00:00:00.000' AS DateTime), 1, N'sd', N'34324', N'3432', N'kishan.suryawanshi@gmail.com', 2, N'324', CAST(N'2017-02-11 16:55:44.307' AS DateTime), 0, 0)
INSERT [dbo].[Enquiry] ([ID], [FirstName], [LastName], [DOB], [GenderID], [Address], [Telephone], [Mobile], [EmailAddress], [CourseID], [Remarks], [EnquiryDate], [StaffID], [StatusID]) VALUES (4, N'dfsd', N'mopdodo', CAST(N'2010-01-01 00:00:00.000' AS DateTime), 1, N'dfg', N'29832489', N'3432', N'kishan.suryawanshi@gmail.com', 2, N'dfsd', CAST(N'2017-02-11 21:17:14.517' AS DateTime), 0, 0)
INSERT [dbo].[Enquiry] ([ID], [FirstName], [LastName], [DOB], [GenderID], [Address], [Telephone], [Mobile], [EmailAddress], [CourseID], [Remarks], [EnquiryDate], [StaffID], [StatusID]) VALUES (7, N'Tanishka', N'Suryawanshi', CAST(N'1984-01-01 00:00:00.000' AS DateTime), 2, N'Kharghar Navi Mumbai', N'+91 9819338426', N'+91 9819338426', N'kchitrangani@gmai.com', 2, N'looking for short term course in FR', CAST(N'2017-02-12 16:26:14.680' AS DateTime), 0, 0)
INSERT [dbo].[Enquiry] ([ID], [FirstName], [LastName], [DOB], [GenderID], [Address], [Telephone], [Mobile], [EmailAddress], [CourseID], [Remarks], [EnquiryDate], [StaffID], [StatusID]) VALUES (8, N'Wolfgang', N'Kaller', CAST(N'1974-12-03 00:00:00.000' AS DateTime), 1, N'19, Keppel Road', N'+65 98765423', N'+65 98765423', N'woflgang@kaller.com', 7, N'Need assistance to learn Angular.', CAST(N'2017-02-13 03:46:22.880' AS DateTime), 0, 0)
INSERT [dbo].[Enquiry] ([ID], [FirstName], [LastName], [DOB], [GenderID], [Address], [Telephone], [Mobile], [EmailAddress], [CourseID], [Remarks], [EnquiryDate], [StaffID], [StatusID]) VALUES (9, N'Prabhu', N'Ramalingham', CAST(N'1988-02-10 00:00:00.000' AS DateTime), 1, N'Singapore', N'+65 92812454', N'+65 92812454', N'prabhu@mm.com', 2, N'need info about java course', CAST(N'2017-02-13 04:20:22.293' AS DateTime), 0, 0)
SET IDENTITY_INSERT [dbo].[Enquiry] OFF
SET IDENTITY_INSERT [dbo].[Students] ON 

INSERT [dbo].[Students] ([ID], [FirstName], [LastName], [DOB ], [GenderID], [Address], [Telephone], [Mobile], [EmailAddress], [CourseID], [StatusID], [FormNumber], [EnrollmentDate], [CityID], [QualificationID], [BatchID ], [TrackID ], [JoiningDate]) VALUES (1, N'Kishan', N'Suryawanshi                                                                                         ', CAST(N'1980-06-03 00:00:00.000' AS DateTime), 1, N'Mumbai India', N'+91 98920103', N'+91 983459383', N't.2010x25@gmail.com', 1, 1, N'DISHA-0001', CAST(N'2017-02-12 02:49:34.240' AS DateTime), 1, 0, 5, 1, CAST(N'2017-02-13 00:00:00.000' AS DateTime))
INSERT [dbo].[Students] ([ID], [FirstName], [LastName], [DOB ], [GenderID], [Address], [Telephone], [Mobile], [EmailAddress], [CourseID], [StatusID], [FormNumber], [EnrollmentDate], [CityID], [QualificationID], [BatchID ], [TrackID ], [JoiningDate]) VALUES (4, N'Vaibhar', N'Kirtikar                                                                                            ', CAST(N'1984-01-01 00:00:00.000' AS DateTime), 0, N'Mumbai India', N'34324', N'+9198920103', N'vaibhar.kiritikar@gmail.com', 2, 2, N'DIS-0001', CAST(N'2017-02-12 02:49:34.240' AS DateTime), 0, 0, 4, 5, CAST(N'2017-01-03 00:00:00.000' AS DateTime))
INSERT [dbo].[Students] ([ID], [FirstName], [LastName], [DOB ], [GenderID], [Address], [Telephone], [Mobile], [EmailAddress], [CourseID], [StatusID], [FormNumber], [EnrollmentDate], [CityID], [QualificationID], [BatchID ], [TrackID ], [JoiningDate]) VALUES (7, N'Darshan', N'Bhagvant                                                                                            ', CAST(N'2016-04-19 00:00:00.000' AS DateTime), 1, N'Mumbai India', N'+91 9895010231', N'+91 9895010231', N'dharshan@gmail.com', 10, 2, N'DIS-0001', CAST(N'2017-02-13 04:05:20.990' AS DateTime), 0, 0, 0, 0, CAST(N'2017-02-20 00:00:00.000' AS DateTime))
INSERT [dbo].[Students] ([ID], [FirstName], [LastName], [DOB ], [GenderID], [Address], [Telephone], [Mobile], [EmailAddress], [CourseID], [StatusID], [FormNumber], [EnrollmentDate], [CityID], [QualificationID], [BatchID ], [TrackID ], [JoiningDate]) VALUES (8, N'Piyush', N'Munde                                                                                               ', CAST(N'1991-01-21 00:00:00.000' AS DateTime), 1, N'Balajinagar Pune', N'+91 9847339342', N'+91 9847339342', N'piyush.munde@gmail.com', 2, 2, N'DIS-0001', CAST(N'2017-02-13 04:13:48.587' AS DateTime), 0, 0, 0, 0, CAST(N'2017-02-08 00:00:00.000' AS DateTime))
INSERT [dbo].[Students] ([ID], [FirstName], [LastName], [DOB ], [GenderID], [Address], [Telephone], [Mobile], [EmailAddress], [CourseID], [StatusID], [FormNumber], [EnrollmentDate], [CityID], [QualificationID], [BatchID ], [TrackID ], [JoiningDate]) VALUES (9, N'Aviraj', N'Patil                                                                                               ', CAST(N'1982-02-01 00:00:00.000' AS DateTime), 1, N'Mumbai India', N'+91 9892009876', N'+91 9892009876', N'aviraj@daysleeper.com', 8, 2, N'DIS-0001', CAST(N'2017-02-13 20:22:31.770' AS DateTime), 2, 0, 2, 2, CAST(N'2017-02-20 00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[Students] OFF
SET IDENTITY_INSERT [dbo].[Tracks] ON 

INSERT [dbo].[Tracks] ([ID], [TrackName], [CreateDate], [AddedBy]) VALUES (2, N'10.00-11.00 AM', CAST(N'2017-02-12 21:47:58.507' AS DateTime), 1)
INSERT [dbo].[Tracks] ([ID], [TrackName], [CreateDate], [AddedBy]) VALUES (3, N'11.00-12.00 AM', CAST(N'2017-02-12 21:48:12.030' AS DateTime), 1)
INSERT [dbo].[Tracks] ([ID], [TrackName], [CreateDate], [AddedBy]) VALUES (4, N'11.30-12.30 PM', CAST(N'2017-02-12 21:49:30.830' AS DateTime), 1)
INSERT [dbo].[Tracks] ([ID], [TrackName], [CreateDate], [AddedBy]) VALUES (5, N'12.30-13.30 PM', CAST(N'2017-02-12 21:49:45.377' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Tracks] OFF
INSERT [dbo].[UserRoles] ([ID], [RoleName], [RoleDescription]) VALUES (1, N'Admin', N'Admin role')
INSERT [dbo].[UserRoles] ([ID], [RoleName], [RoleDescription]) VALUES (2, N'Operator', N'Operator role')
INSERT [dbo].[UserRoles] ([ID], [RoleName], [RoleDescription]) VALUES (3, N'Employee', N'Employees in DISHA')
INSERT [dbo].[UserRoles] ([ID], [RoleName], [RoleDescription]) VALUES (4, N'Student', N'Students in DISHA')
