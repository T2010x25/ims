# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Institute Management System - Student information system, an application useful for small to medium scale educational organizations such as Computer training institutes , Yoga training institutes, Playschools, music & dance training institutes.Going forward idea is to implement this application as a framework for medium and large scale educational institutes. 

* Beta - Very few modules are completed. Development is in progress, but overall structure and
  technology will remain same till the end.

* Asp.NET MVC 5, C#, SQL Server, Autofac, JQuery, Design Patterns.

### How do I get set up? ###

* Download the source code, Open in any edition of Visual Studio 2015. Install the nugget
  package dependencies. 
* Set as Ims.Web as Startup Project and Run. 
* http://localhost:54661/ is a public facing application.
  Student can view the courses, and submit an enquiry.

* http://localhost:54661/admin/account/login - use this URL to run the Admin module. 
  This module is used by training institute employees to view the submitted enquires, 
  student enrollment, Course fees payments, employee management, accounting, eLearning and 
  reports.
*
* User name : demo@disha.co.in
* password  : abcd1234
  
* Nuget Package Dependencies 
* Database ImsDB is inside App_Data directory.
* How to run tests - Run the unit test from Ims.Testing project.
## * Please refer documentation.docx file in Ims.Web project for modules and mockups in the system. ##

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact