﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ims.web.Models
{
    public class CoursesViewModel
    {
        public int ID { get; set; } 
        public string CourseTitle { get; set; }
        public DateTime CreateDate { get; set; }
        public int AddedBy { get; set; }
        public string CourseDescription { get; set; }
        public string IconUrl { get; set; }
        public int StatusID { get; set; }
        public string StatusDescription { get; set; }
    }
}