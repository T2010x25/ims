﻿using Ims.web.BaseModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ims.web.Models
{
    public class EnquiryViewModel : StudentBase
    {
        [Display(Name = "Enquiry Date")]
        public DateTime EnquiryDate { get; set; }
        [Display(Name = "Remarks")]
        public string Remarks { get; set; }
      
    }
}