﻿using Framework.Data.Repositories;
using Framework.Entities;
using Ims.web.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ims.web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IEntityBaseRepository<Students> studentsRepository;

        public HomeController(IEntityBaseRepository<Students>studentsrepository)
        {
            studentsRepository = studentsrepository;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {             
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
 
    }
}