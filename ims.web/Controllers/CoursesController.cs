﻿using Framework.Data.Repositories;
using Framework.Entities;
using Ims.web.Extensions;
using Ims.web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ims.web.Controllers
{
    public class CoursesController : Controller
    {
        private readonly IEntityBaseRepository<Courses> coursesRepository;
        public CoursesController(IEntityBaseRepository<Courses> coursesrepository)
        {
            this.coursesRepository = coursesrepository;
        }
        [AllowAnonymous]
        public ActionResult Index()
        {
            var courses = coursesRepository.GetAll().ToList();
            List<CoursesViewModel> coursesVM = new List<CoursesViewModel>();
            courses.MapCoursesViewModel(coursesVM);
            return View(coursesVM);
        }
        [AllowAnonymous]
        public ActionResult CourseDetails(int id) 
        {
              

            return View();
        }
    }
}