﻿using Framework.Data.Repositories;
using Framework.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ims.web.Controllers
{
    // security to this controller // to do...
    public class DataController : Controller
    {
        private readonly IEntityBaseRepository<Courses> coursesRepository;
        public DataController(IEntityBaseRepository<Courses> coursesrepository)
        {
            this.coursesRepository = coursesrepository;
        }
        
        public ActionResult GetCourses(string keyword)
        {
            var courses = coursesRepository.GetAll()                 
                .Where(c => c.CourseTitle.Contains(keyword))
                .OrderBy(c => c.CourseTitle)
                .Select(c => new { label = c.CourseTitle, value = c.ID })
                .ToList();

            return Json(courses,JsonRequestBehavior.AllowGet);
        }
        public ActionResult TobeImplemented(string keyword)
        {
            List<Templist> list = new List<Templist>();
            list.Add(new Templist
            {
                ID = 2,
                Text = "To be implemented"
            });
            var t = list.Select(l => new { label = l.Text, value = l.ID }).ToList();
            return Json(t, JsonRequestBehavior.AllowGet);
        }
        class Templist
        {
            public int ID { get; set; }
            public string Text { get; set; }
        }
    }
}