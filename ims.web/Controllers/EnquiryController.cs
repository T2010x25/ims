﻿using Framework.Data.Infrastructure;
using Framework.Data.Repositories;
using Framework.Entities;
using Ims.web.Core;
using Ims.web.Extensions;
using Ims.web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ims.web.Controllers
{
    public class EnquiryController : ImsControllerBase
    {
        private readonly IEntityBaseRepository<Enquiry> enquiryRepository;
        
        public EnquiryController(IEntityBaseRepository<Enquiry> enquiryrepository,
                IEntityBaseRepository<ErrorLog>errorRepository,IUnitOfWork unitOfWork) 
            :base(errorRepository,unitOfWork)

        {
            this.enquiryRepository = enquiryrepository;
            
        }
            
        [AllowAnonymous]
        public ActionResult AddEnquiry()
        {
           return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult AddEnquiry(EnquiryViewModel enquiryDetails)
        {
            Enquiry enquiry = new Enquiry();
            enquiry.UpdateEnquiry(enquiryDetails);
            enquiryRepository.Add(enquiry);
            unitOfWork.Commit();
            // to do handle exception by passing object to below method.
            return RedirectToAction("Message");
        }
        public ActionResult Message()
        {
            return View("ThankYou");
        }
    }
}