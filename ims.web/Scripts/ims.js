﻿var commonmodule = {
    init: function () {
        $('#CourseName').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/data/getcourses",
                    dataType: "json",
                    data: {
                        keyword: request.term
                    },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                label: item.label,
                                value: item.value
                            }
                        }));
                    }
                });
            },
            select: function (event, ui) {                
                $("#CourseName").val(ui.item.label);
                $("#CourseID").val(ui.item.value);
                return false;
            }
        });
        $("#DOB").datepicker();
    }
};

var enrollmodule = {
    init: function () {
        commonmodule.init();
        $("#JoiningDate").datepicker();
        $('#CityName').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/data/TobeImplemented",
                    dataType: "json",
                    data: {
                        keyword: request.term
                    },
                    success: function (data) {
                        debugger;
                        response($.map(data, function (item) {
                            return {
                                label: item.label,
                                value: item.value
                            }
                        }));
                    }
                });
            },
            select: function (event, ui) {
                $("#CityName").val(ui.item.label);
                $("#CityID").val(ui.item.value);
                return false;
            }
        });

        $('#BatchName').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/data/TobeImplemented",
                    dataType: "json",
                    data: {
                        keyword: request.term
                    },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                label: item.label,
                                value: item.value
                            }
                        }));
                    }
                });
            },
            select: function (event, ui) {
                $("#BatchName").val(ui.item.label);
                $("#BatchID").val(ui.item.value);
                return false;
            }
        });

        $('#TrackName').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/data/TobeImplemented",
                    dataType: "json",
                    data: {
                        keyword: request.term
                    },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                label: item.label,
                                value: item.value
                            }
                        }));
                    }
                });
            },
            select: function (event, ui) {
                $("#TrackName").val(ui.item.label);
                $("#TrackID").val(ui.item.value);
                return false;
            }
        });
    }
};



 