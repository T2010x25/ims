﻿using Framework.Data.Infrastructure;
using Framework.Data.Repositories;
using System;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using Ims.web.Extensions;
using System.Web.Mvc;
using Framework.Entities;

namespace Ims.web.Core
{
    public class ImsControllerBase : Controller
    {
        protected readonly IEntityBaseRepository<ErrorLog> errorsRepository;
        protected readonly IUnitOfWork unitOfWork;

        public ImsControllerBase(IEntityBaseRepository<ErrorLog> errorsrepository, IUnitOfWork unitofwork)
        {
            errorsRepository = errorsrepository;
            unitOfWork = unitofwork;
        }

        //public ApiControllerBase(IDataRepositoryFactory dataRepositoryFactory, IEntityBaseRepository<Error> errorsRepository, IUnitOfWork unitOfWork)
        //{
        //    _errorsRepository = errorsRepository;
        //    _unitOfWork = unitOfWork;
        //}

        protected HttpResponseMessage CreateHttpResponse(HttpRequestMessage request, Func<HttpResponseMessage> function)
        {
            HttpResponseMessage response = null;

            try
            {
                response = function.Invoke();
            }
            catch (DbUpdateException ex)
            {
                LogError(ex);
                response = request.CreateResponse(HttpStatusCode.BadRequest, ex.InnerException.Message);
            }
            catch (Exception ex)
            {
                LogError(ex);
                response = request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

            return response;
        }
        private void LogError(Exception ex)
        {
            try
            {
                ErrorLog err = new ErrorLog()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                    CreateDate = DateTime.Now
                };
                errorsRepository.Add(err);
                unitOfWork.Commit();
            }
            catch { }
        }
    }
}