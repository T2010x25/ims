﻿using Ims.web.BaseModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ims.web.Areas.Admin.Models
{
    public class StudentsViewModel : StudentBase
    {
        public string FormNumber { get; set; }
        public DateTime EnrollmentDate { get; set; }

        public int CityID { get; set; }
        [Required]
        [Display(Name = "City")]
        public string CityName { get; set; }

        public int QualificationID { get; set; }

        public int BatchID { get; set; }
        [Required]
        [Display(Name = "Batch")]
        public string BatchName { get; set; }

        public int TrackID { get; set; }
        [Required]
        [Display(Name = "Track")]
        public string TrackName { get; set; }

        [Required]
        [Display(Name = "Joining Date")]
        public DateTime JoiningDate { get; set; }

        public StudentsViewModel()
        {
            
        }
    }
}