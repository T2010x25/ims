﻿using Framework.Data.Infrastructure;
using Framework.Data.Repositories;
using Framework.Entities;
using Ims.web.Areas.Admin.Models;
using Ims.web.Core;
using Ims.web.Extensions;
using Ims.web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ims.web.Areas.Admin.Controllers
{
    public class StudentsController : ImsControllerBase
    {
        private readonly IEntityBaseRepository<Students> studentsRepository;
        private readonly IEntityBaseRepository<Enquiry> enquiryRepository;

        public StudentsController(IEntityBaseRepository<Students> studentsrepository,
                IEntityBaseRepository<ErrorLog> errorRepository, IUnitOfWork unitOfWork,
                IEntityBaseRepository<Enquiry>enquiryrepository) 
            :base(errorRepository,unitOfWork)

        {
            this.studentsRepository = studentsrepository;
            this.enquiryRepository = enquiryrepository;

        }

        public ActionResult Index()
        {
            var students = studentsRepository.GetAll()
                .OrderByDescending(o =>o.EnrollmentDate)
                .ToList();
            // Paging logic to be implemented.....

            List<StudentsViewModel> studentVM = new List<StudentsViewModel>();
            students.MapStudentViewModel(studentVM);
            return View(studentVM);
        }

        public ActionResult Enroll()
        { 
            return View();
        }

        [HttpPost]
        public ActionResult Enroll(StudentsViewModel studentVM)
        {
            Students student = new Students();
            student.UpdateEnrollment(studentVM);
            studentsRepository.Add(student);
            unitOfWork.Commit();
            // to do handle exception cases by passing paramter to the following view.
            return RedirectToAction("Message");            
        }

        public ActionResult Enquries(int page = 1, int pageSize = 10, string filter = null)
        {
            int currentPage = page;
            int currentPageSize = pageSize;
            var enquries = enquiryRepository.GetAll()
                        .OrderByDescending(e => e.ID)
                        .Take(currentPageSize).ToList();
            List<EnquiryViewModel> enquriesVM = new List<web.Models.EnquiryViewModel>();
            enquries.MapEnquiryViewModel(enquriesVM);

            return View(enquriesVM);
        }
        public ActionResult Message()
        {
            return View("Confirmation");
        }
    }
}