﻿using System.Web.Mvc;
using Framework.Services;
using System.Web.Security;
using Ims.web.Areas.Admin.Models;

namespace Ims.web.Areas.Admin.Controllers
{    
    public class AccountController : Controller
    {
        private readonly IAuthenticateService authService;
        public AccountController(IAuthenticateService authservice)
        {
            this.authService = authservice;
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginViewModel user)
        {
            if (ModelState.IsValid)
            {
                AuthContext authcontext = authService.ValidateUser
                                            (user.Email, user.Password);
                if (authcontext.User != null)
                {
                    FormsAuthentication.SetAuthCookie(
                                    user.Email, user.RememberMe);
                    return RedirectToAction("Index", "AdminHome");
                }
                else
                {
                    ModelState.AddModelError("", "Login information is incorrect!");
                }
            }
            return View();
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }

        public ActionResult Register()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Register(RegisterViewModel registerVM)
        {
            if(ModelState.IsValid)
            {
                var user = authService.CreateUser(registerVM.Email, registerVM.Password, registerVM.RoleID);
                if(user != null)
                {

                }
            }
            return View();
        }
    }
}