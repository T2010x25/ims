﻿using Ims.web.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ims.web.Areas.Admin.Controllers
{
    public class MastersController :  Controller // ImsControllerBase 
    {

        public MastersController()
        {
             
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Batches()
        {
            return View();
        }

        public ActionResult Tracks()
        {
            return View();
        }

        public ActionResult Branches()
        {
            return View();
        }

        public ActionResult Expenses()
        {
            return View();
        }

        public ActionResult Income()
        {
            return View();
        }

        public ActionResult Banks()
        {
            return View();
        }

        public ActionResult Cities()
        {
            return View();
        }

    }
}