using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc5;
using Framework.Data.Infrastructure;
using Framework.Data.Repositories;
using System.Data.Entity;
using Framework.Data;
using Ims.web.Core;
using Framework.Services;

namespace Ims.web
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IUnitOfWork, UnitOfWork>
                        (new HierarchicalLifetimeManager());
            


            container.RegisterType(typeof(IEntityBaseRepository<>), typeof(EntityBaseRepository<>));
            container.RegisterType<IDbFactory, DbFactory>();
            container.RegisterType<DbContext, ImsContext>();
            container.RegisterType<DbContext, ImsContext>();
            // container.RegisterType<IDataRepositoryFactory, DataRepositoryFactory>();
            container.RegisterType<IAuthenticateService, AuthenticateService>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}