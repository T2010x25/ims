﻿using Autofac;
using Autofac.Integration.Mvc;
using Framework.Data;
using Framework.Data.Infrastructure;
using Framework.Data.Repositories;
using Framework.Services;
using Ims.web.Core;
using System.Data.Entity;
using System.Web.Mvc;

namespace Ims.web
{
    public class AutofacConfig
    {
 
        public static void RegisterServices()
        {
            ContainerBuilder builder = new ContainerBuilder();
            
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            // EF IMSContext
            builder.RegisterType<ImsContext>()
                   .As<DbContext>()
                   .InstancePerRequest();

            builder.RegisterType<DbFactory>()
                .As<IDbFactory>()
                .InstancePerRequest();

            builder.RegisterType<UnitOfWork>()
                .As<IUnitOfWork>()
                .InstancePerRequest();

            builder.RegisterGeneric(typeof(EntityBaseRepository<>))
                   .As(typeof(IEntityBaseRepository<>))
                   .InstancePerRequest();

            // Services
            builder.RegisterType<EncryptionService>()
                .As<IEncryptionService>()
                .InstancePerRequest();

            builder.RegisterType<AuthenticateService>()
                .As<IAuthenticateService>()
                .InstancePerRequest();

            // Generic Data Repository Factory
            builder.RegisterType<DataRepositoryFactory>()
                .As<IDataRepositoryFactory>().InstancePerRequest();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));            
        }
    }
}