﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ims.web.BaseModels
{
    public class StudentBase
    {
        public int ID { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required]
        [Display(Name = "Date of Birth")]
        public DateTime DOB { get; set; }
        [Display(Name = "Gender")]
        public int GenderID { get; set; }
        [Display(Name = "Address")]
        public string Address { get; set; }
        [Display(Name = "Telephone")]
        public string Telephone { get; set; }
        [Required]
        [Display(Name = "Mobile")]
        public string Mobile { get; set; }
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }
        public int CourseID { get; set; }
        [Required]
        [Display(Name = "Course")]
        public string CourseName { get; set; }
        public int StaffID { get; set; }
        public int StatusID { get; set; }
    }
}