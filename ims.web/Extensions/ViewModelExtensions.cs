﻿using Framework.Entities;
using Ims.web.Areas.Admin.Models;
using Ims.web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ims.web.Extensions
{
    public static class ViewModelExtensions
    {
        public static void MapEnquiryViewModel(this List<Enquiry> enquiries  , List<EnquiryViewModel> enquiryVM)
        {
            foreach (var e in enquiries)
            {
                enquiryVM.Add(new EnquiryViewModel
                {
                    FirstName = e.FirstName,
                    LastName = e.LastName,
                    EmailAddress = e.EmailAddress,
                    Mobile = e.Mobile,
                    EnquiryDate = e.EnquiryDate,
                    CourseName = e.Course.CourseTitle,
                    CourseID = e.CourseID                    
                });
            }
        }

        public static void MapStudentViewModel(this List<Students> students, List<StudentsViewModel> studentsVM)
        {
            foreach (var e in students)
            {
                studentsVM.Add(new StudentsViewModel
                {
                    ID = e.ID,
                    FirstName = e.FirstName,
                    LastName = e.LastName,
                    EmailAddress = e.EmailAddress,
                    Mobile = e.Mobile,
                    EnrollmentDate = e.EnrollmentDate,
                    CourseName = e.Course.CourseTitle,
                    CourseID = e.CourseID
                });
            }
        }

        public static void MapCoursesViewModel(this List<Courses> courses, List<CoursesViewModel> coursesVM)
        {
            foreach (var c in courses)
            {
                coursesVM.Add(new CoursesViewModel
                {
                    ID = c.ID,
                    CourseTitle = c.CourseTitle,
                    CourseDescription = c.CourseDescription,
                    IconUrl = c.IconUrl
                });
            }
        }
    }
}