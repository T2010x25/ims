﻿using Framework.Entities;
using Ims.web.Areas.Admin.Models;
using Ims.web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ims.web.Extensions
{
    public static class EntitiesExtensions
    {
        public static void UpdateEnquiry(this Enquiry enquiry, EnquiryViewModel enquiryVm)
        {
            enquiry.FirstName = enquiryVm.FirstName;
            enquiry.LastName = enquiryVm.LastName;
            enquiry.GenderID = enquiryVm.GenderID;
            enquiry.CourseID = enquiryVm.CourseID;
            enquiry.EmailAddress = enquiryVm.EmailAddress;
            enquiry.DOB = enquiryVm.DOB;
            enquiry.Address = enquiryVm.Address;
            enquiry.StaffID = enquiryVm.StaffID;
            enquiry.Telephone = enquiryVm.Telephone;
            enquiry.Mobile = enquiryVm.Mobile;
            enquiry.Remarks = enquiryVm.Remarks;
            enquiry.EnquiryDate = DateTime.Now;
            enquiry.Address = enquiryVm.Address;
        }

        public static void UpdateEnrollment(this Students entrollstudent, StudentsViewModel studentsVm)
        {
            entrollstudent.FirstName = studentsVm.FirstName;
            entrollstudent.LastName = studentsVm.LastName;
            entrollstudent.GenderID = studentsVm.GenderID;
            entrollstudent.CourseID = studentsVm.CourseID;
            entrollstudent.EmailAddress = studentsVm.EmailAddress;
            entrollstudent.DOB = studentsVm.DOB;
            entrollstudent.Address = studentsVm.Address;            
            entrollstudent.Telephone = studentsVm.Telephone;
            entrollstudent.Mobile = studentsVm.Mobile;
            entrollstudent.EnrollmentDate = DateTime.Now;
            entrollstudent.CityID = studentsVm.CityID;
            entrollstudent.JoiningDate = studentsVm.JoiningDate;
            entrollstudent.TrackID = studentsVm.TrackID;
            entrollstudent.StatusID = 2;
            entrollstudent.QualificationID = studentsVm.QualificationID;
            entrollstudent.FormNumber = studentsVm.FormNumber;
            entrollstudent.BatchID = studentsVm.BatchID;
            entrollstudent.FormNumber = "DIS-0001";
        }
    }
}