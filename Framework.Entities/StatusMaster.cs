﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Entities
{
    public class StatusMaster :IEntityBase
    {
        public int ID { get; set; }
        public string StatusTitle { get; set; } 
    }
}
