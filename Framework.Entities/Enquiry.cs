﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Entities
{
    [Table("Enquiry")]
    public class Enquiry : IEntityBase
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public int GenderID { get; set; }
        public string Address { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string EmailAddress { get; set; }
        public int CourseID { get; set; }
        public string Remarks { get; set; }
        public DateTime EnquiryDate { get; set; }
        public int StaffID { get; set; }
        public int StatusID { get; set; }
        public virtual Courses Course { get; set; }
    }
}
