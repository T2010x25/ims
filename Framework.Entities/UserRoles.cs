﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Entities
{
    public class UserRoles : IEntityBase
    {
        public int ID { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
    }
}
