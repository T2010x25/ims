﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Entities
{
    public class Batches :IEntityBase
    {
        public int ID { get; set; }
        public string BatchName { get; set; }
        public DateTime CreateDate { get; set; }
        public int AddedBy { get; set; }
    }
}
