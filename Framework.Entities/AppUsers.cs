﻿using System;

namespace Framework.Entities
{
    /// <summary>
    /// Application users .. Computer institute staff members.
    /// </summary>
    public class AppUsers : IEntityBase
    {
        public int ID { get; set; }
        public string EmailAddress { get; set; }
        public string PasswordHash { get; set; }
        public string Salt { get; set; }
        public bool IsLocked { get; set; }
        public DateTime DateCreated { get; set; }
        public int RoleID { get; set; }
    }
}
