﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Entities
{
    public class Courses :IEntityBase
    {
        public int ID { get; set; }
        public string CourseTitle { get; set; }
        public DateTime CreateDate { get; set; }
        public int AddedBy { get; set; }
        public string CourseDescription { get; set; }
        public string IconUrl { get; set; }
        public int StatusID { get; set; }
        public virtual StatusMaster Status { get; set; }
    }
}
