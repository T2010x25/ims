﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Entities
{
   public class Students:IEntityBase
    {
        public int ID { get; set; }
        public string FirstName { get; set; }       
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public int GenderID { get; set; }
        public string Address { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string EmailAddress { get; set; }
        public int CourseID { get; set; }                
        public int StatusID { get; set; }
        public string FormNumber { get; set; }
        public DateTime EnrollmentDate { get; set; }
        public int CityID { get; set; }
        public int QualificationID { get; set; }
        public int BatchID { get; set; }
        public int TrackID { get; set; }
        public DateTime JoiningDate { get; set; }

        public virtual Courses Course { get; set; }
        public virtual Batches Batch { get; set; }
        public virtual Cities City { get; set; }
        public virtual Tracks Track { get; set; }

    }
}
