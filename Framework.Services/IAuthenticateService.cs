﻿using Framework.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Services
{
    public interface IAuthenticateService
    {
        AuthContext ValidateUser(string email, string password);
        AppUsers CreateUser(string username, string password, int role);
    }
}
