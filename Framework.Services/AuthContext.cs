﻿using Framework.Entities;
using System.Security.Principal;

namespace Framework.Services
{
    public class AuthContext
    {
        public IPrincipal Principal { get; set; } 
        public AppUsers User { get; set; }
        public bool IsValid ()
        {
            return Principal != null;
        }

    }
}
