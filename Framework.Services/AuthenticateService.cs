﻿using Framework.Data.Extensions;
using Framework.Data.Infrastructure;
using Framework.Data.Repositories;
using Framework.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Services
{
    public class AuthenticateService :IAuthenticateService
    {
        private readonly IEntityBaseRepository<AppUsers> appuserRepository;
        private readonly IEntityBaseRepository<UserRoles> userroleRepository;
        private readonly IEncryptionService encryptionService;
        private readonly IUnitOfWork unitofWork;

        public AuthenticateService(IEntityBaseRepository<AppUsers> appuserrepository,
                    IEntityBaseRepository<UserRoles> userrolerepository,
                    IEncryptionService encryptionservice, IUnitOfWork unitofwork )
        {
            this.appuserRepository = appuserrepository;
            this.userroleRepository = userrolerepository;
            this.encryptionService = encryptionservice;
            this.unitofWork = unitofwork;
        }

        public AuthContext ValidateUser(string email, string password)
        {
            var authContext = new AuthContext();
            var user = appuserRepository.GetSingleByUsername(email);
            if (user != null && isUserValid(user,password))
            {
                authContext.User = user;
                var identity = new GenericIdentity(user.EmailAddress);
                var roleinfo = userroleRepository.GetSingle(user.RoleID);
                authContext.Principal = new GenericPrincipal
                    (
                        identity, new[] { roleinfo.RoleName }
                    );
            }
            return authContext;
        }

        public AppUsers CreateUser(string username,string password, int role)
        {
            var existingUser = appuserRepository.GetSingleByUsername(username);

            if (existingUser != null)
            {
                throw new Exception("Username is already in use");
            }

            var passwordSalt = encryptionService.CreateSalt();

            var user = new AppUsers()
            {
                EmailAddress = username,
                Salt = passwordSalt,
                IsLocked = false,
                PasswordHash = encryptionService.EncryptPassword(password, passwordSalt),
                DateCreated = DateTime.Now,
                RoleID = role
            };

            appuserRepository.Add(user);
            unitofWork.Commit();
            return user;
        }


        private bool isUserValid(AppUsers user, string password)
        {
            if(isPasswordValid(user,password))
            {
                return !user.IsLocked;
            }
            return false;
        }

        private bool isPasswordValid(AppUsers user, string password)
        {
            return string.Equals
                (encryptionService.EncryptPassword(password, user.Salt),user.PasswordHash);
        }
    }
}
